package com.langnerd.moolah.endpoints;

import com.langnerd.moolah.generated.springmvc.definitions.SentimentForm;
import com.langnerd.moolah.generated.springmvc.sentiment.SentimentHandler;
import java.util.concurrent.CompletionStage;
import com.langnerd.moolah.nlp.SentimentAnalyser;
import org.springframework.stereotype.Component;

@Component
public class DefaultSentimentHandler implements SentimentHandler {

  private final SentimentAnalyser sentimentAnalyser;

  public DefaultSentimentHandler(
      SentimentAnalyser sentimentAnalyser) {
    this.sentimentAnalyser = sentimentAnalyser;
  }

  @Override
  public CompletionStage<GetSentimentResponse> getSentiment(SentimentForm body) {

    return null;
  }
}
