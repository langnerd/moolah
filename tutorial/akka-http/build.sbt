import Dependencies._

libraryDependencies ++= akka ++ Cats.akka ++ Circe.akka

guardrailTasks in Compile := List(
  ScalaServer(
    specPath = baseDirectory.value / "../../core/src/main/resources/api.yaml",
    pkg = "com.langnerd.moolah.generated.akka",
    framework = "akka-http",
    tracing = false
  )
)