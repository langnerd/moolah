import Dependencies._

name := "moolah"
version := "0.1"
scalaVersion := "2.13.3"

lazy val root = (project in file("."))
  .aggregate(core, akkaHttp)

lazy val core = project in file("core")
lazy val akkaHttp = (project in file("tutorial/akka-http"))
  .settings(
    scalacOptions += "-Ypartial-unification"
  )

libraryDependencies ++= http4s ++ Circe.http4s ++ Cats.http4s :+ logback :+ specs2

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Xfatal-warnings",
)

guardrailTasks in Compile := List(
  ScalaServer(
    specPath = baseDirectory.value / "core/src/main/resources/api.yaml",
    pkg = "com.langnerd.moolah.generated",
    framework = "http4s",
    tracing = false
  )
)
