# moolah

Extraction of financial com.langnerd.moolah.data from tweets, sentiment analysis and comparison with historical financial com.langnerd.moolah.data. The project's aim is to assess Stanford NLP capabilities and its sole purpose is educational.

> "moolah" = money, 1930s (originally US) of unknown origin.
>
> [Google]

## Text Analysis
* [Sentiment Analyser using Stanford CoreNLP](core/src/main/java/com/langnerd/moolah/nlp/impl/DefaultSentimentAnalyser.java)

### API
TBD

## Datasets

I've used [StockNet data set](https://github.com/yumoxu/stocknet-dataset) which tracks tweets and daily prices for 88 selected stocks
during 2014 - 2016.

Raw tweets and prices can be uploaded into a MySQL database:
* [schema](core/src/main/resources/schema.sql)
* [zipped data (CSV)](core/src/main/resources/com.langnerd.moolah.data.tar.gz)

## Resources

* [On the Importance of Text Analysis for Stock Price Prediction](https://nlp.stanford.edu/pubs/lrec2014-stock.pdf) (PDF)
* [Extracting Strong Sentiment Trends from Twitter](https://nlp.stanford.edu/courses/cs224n/2011/reports/patlai.pdf) (PDF)
* [Corpus that aligns descriptions of financial events with changes in stock prices](https://nlp.stanford.edu/pubs/stock-event.html)
* [SentiStrength: Analysis of short texts and emoticons](http://sentistrength.wlv.ac.uk/)
