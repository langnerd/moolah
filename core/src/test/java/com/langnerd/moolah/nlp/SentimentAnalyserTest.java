package com.langnerd.moolah.nlp;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.langnerd.moolah.ConsoleApplication;
import com.langnerd.moolah.model.Sentiment;
import com.langnerd.moolah.model.TweetSentiment;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConsoleApplication.class)
public class SentimentAnalyserTest {

  @Autowired
  private SentimentAnalyser sentimentAnalyser;

  @Test
  public void negative() {
    verifySentiment("negative", Sentiment.NEGATIVE);
  }

  @Test
  public void neutral() {
    verifySentiment("neutral", Sentiment.NEUTRAL);
  }

  @Test
  public void positive() {
    verifySentiment("positive", Sentiment.POSITIVE);
  }

  private void verifySentiment(String filename, Sentiment expectedSentiment) {
    try (InputStream inputStream = getClass().getClassLoader()
        .getResourceAsStream(String.format("tweets/%s.txt", filename));
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
      String tweet;
      while ((tweet = bufferedReader.readLine()) != null) {
        final Optional<TweetSentiment> sentiment = sentimentAnalyser.getSentiment(tweet);
        assertTrue(sentiment.isPresent());
        assertThat(tweet, sentiment.get().getSentiment(), is(expectedSentiment));
        assertTrue(!sentiment.get().getSentenceScores().isEmpty());
      }
    } catch (Exception ex) {
      fail(String.format("Failed to read resource file %s, exception: %s", filename, ex));
    }
  }
}
