package com.langnerd.moolah;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan
@SpringBootApplication
public class ConsoleApplication implements CommandLineRunner {

  private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleApplication.class);

  public static void main(String[] args) {
    LOGGER.info("Starting the app ..");
    SpringApplication.run(ConsoleApplication.class, args);
    LOGGER.info("App started.");
  }
  @Override
  public void run(String... args) {
    LOGGER.info("EXECUTING : command line runner");
  }
}
