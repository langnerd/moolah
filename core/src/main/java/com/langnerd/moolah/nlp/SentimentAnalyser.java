package com.langnerd.moolah.nlp;

import com.langnerd.moolah.model.TweetSentiment;
import java.util.Optional;

public interface SentimentAnalyser {

  Optional<TweetSentiment> getSentiment(String text);
}
