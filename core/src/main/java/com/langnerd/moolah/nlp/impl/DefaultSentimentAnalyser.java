package com.langnerd.moolah.nlp.impl;

import com.langnerd.moolah.model.ScoreDistribution;
import com.langnerd.moolah.model.SentenceScore;
import com.langnerd.moolah.model.Sentiment;
import com.langnerd.moolah.model.Tweet;
import com.langnerd.moolah.model.TweetSentiment;
import com.langnerd.moolah.model.WeightedSentiment;
import com.langnerd.moolah.nlp.SentimentAnalyser;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Properties;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.ejml.simple.SimpleMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DefaultSentimentAnalyser implements SentimentAnalyser {

  private final Logger logger = LoggerFactory.getLogger(DefaultSentimentAnalyser.class);

  private final StanfordCoreNLP nlp;

  public DefaultSentimentAnalyser() {
    Properties props = new Properties();
    props.setProperty("annotators", "tokenize, ssplit, pos, lemma, parse, sentiment");
    nlp = new StanfordCoreNLP(props);
  }

  @Override
  public Optional<TweetSentiment> getSentiment(String text) {
    try {
      Tweet tweet = parseTweet(text);

      // Evaluate tweet text
      final List<WeightedSentiment> sentiments = sentenceSentiments(tweet);

      // Return the calculated sentiment of the entire text, if any.
      return calculateSentiment(sentiments);
    } catch (Exception e) {
      logger.error(String.format("Failed to process tweet %s: %s", text, e));
      return Optional.empty();
    }
  }

  private List<WeightedSentiment> sentenceSentiments(Tweet tweet) {
    return processSentenceAnnotation(nlp.process(tweet.getText()),
        (sentence, tree) -> {
          int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
          int weight = getWeight(sentence.toString(), tweet.getHashTags());
          SentenceScore sentenceScore = new SentenceScore(sentiment, scoreDistribution(tree));
          return new WeightedSentiment(sentiment, weight, sentenceScore);
        });
  }

  private Tweet parseTweet(String text) {
    return new Tweet(text.toLowerCase().replace("#", ""), hashTags(text));
  }

  private Set<String> hashTags(String text) {
    return Arrays.stream(text.toLowerCase().split("\\s+"))
        .filter(s -> s.startsWith("#"))
        .map(s -> s.replace("#", ""))
        .collect(Collectors.toSet());
  }

  private <T> List<T> processSentenceAnnotation(Annotation annotation,
      BiFunction<CoreMap, Tree, T> resultFunction) {
    try {
      return annotation
          .get(SentencesAnnotation.class)
          .stream().map(sentence -> processSentence(resultFunction, sentence))
          .filter(Optional::isPresent)
          .map(Optional::get)
          .collect(Collectors.toList());
    } catch (Exception ex) {
      logger
          .error(String.format("Failed to process annotation: %s, exception: %s", annotation, ex));
      return Collections.emptyList();
    }
  }

  private <T> Optional<T> processSentence(BiFunction<CoreMap, Tree, T> resultFunction,
      CoreMap sentence) {
    try {
      Tree tree = sentence.get(SentimentAnnotatedTree.class);
      return Optional.of(resultFunction.apply(sentence, tree));
    } catch (Exception ex) {
      logger.error(
          String.format("Failed to process sentence: %s, exception: %s", sentence, ex));
      return Optional.empty();
    }
  }

  private Optional<TweetSentiment> calculateSentiment(
      List<WeightedSentiment> weightedSentiments) {

    OptionalInt maybeMax = weightedSentiments.stream()
        .mapToInt(WeightedSentiment::getWeight).max();

    OptionalDouble maybeAvg = maybeMax.stream()
        .mapToDouble(max -> weightedSentiments.stream().filter(ws -> ws.getWeight() == max)
            .collect(averagingWeighted(WeightedSentiment::getSentimentOrdinal,
                WeightedSentiment::getWeight)))
        .findFirst();

    return maybeAvg.stream()
        .mapToObj(avg -> Sentiment.values()[(int) Math.round(avg)])
        .findFirst().map(sentiment -> new TweetSentiment(sentiment,
            weightedSentiments.stream()
                .map(WeightedSentiment::getSentenceScore)
                .collect(Collectors.toList()))
        );
  }

  private int getWeight(String text, Set<String> hashTags) {
    final int matchingHashTagsSize = hashTags.stream()
        .filter(text::contains)
        .mapToInt(String::length)
        .sum();
    return text.length() + matchingHashTagsSize;
  }

  private ScoreDistribution scoreDistribution(Tree tree) {
    final SimpleMatrix sm = RNNCoreAnnotations.getPredictions(tree);
    return new ScoreDistribution(
        sentimentScoreValue(sm.get(4)),
        sentimentScoreValue(sm.get(3)),
        sentimentScoreValue(sm.get(2)),
        sentimentScoreValue(sm.get(1)),
        sentimentScoreValue(sm.get(0))
    );
  }

  private double sentimentScoreValue(double score) {
    return Math.round(score * 100d);
  }

  // Credit: https://stackoverflow.com/questions/40420069/calculate-weighted-average-with-java-8-streams
  private <T> Collector<T, ?, Double> averagingWeighted(ToDoubleFunction<T> valueFunction,
      ToIntFunction<T> weightFunction) {
    class Box {

      double numerator = 0;
      long denominator = 0;
    }
    return Collector.of(
        Box::new,
        (b, e) -> {
          b.numerator += valueFunction.applyAsDouble(e) * weightFunction.applyAsInt(e);
          b.denominator += weightFunction.applyAsInt(e);
        },
        (b1, b2) -> {
          b1.numerator += b2.numerator;
          b1.denominator += b2.denominator;
          return b1;
        },
        b -> b.numerator / b.denominator
    );
  }
}

