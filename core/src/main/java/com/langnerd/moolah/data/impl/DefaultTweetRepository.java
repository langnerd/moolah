package com.langnerd.moolah.data.impl;

import com.langnerd.moolah.data.TweetRepository;
import java.time.Instant;
import java.util.List;
import com.langnerd.moolah.model.Tweet;
import org.springframework.stereotype.Component;

@Component
public class DefaultTweetRepository implements TweetRepository {

  @Override
  public List<Tweet> getTweets(Instant from, Instant to) {
    throw new UnsupportedOperationException("To be implemented");
  }
}
