package com.langnerd.moolah.data;

import java.time.Instant;
import java.util.List;
import com.langnerd.moolah.model.Tweet;

public interface TweetRepository {

  List<Tweet> getTweets(Instant from, Instant to);
}
