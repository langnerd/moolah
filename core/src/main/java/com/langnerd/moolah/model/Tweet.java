package com.langnerd.moolah.model;

import java.util.Objects;
import java.util.Set;

public class Tweet {
  private final String text;

  private final Set<String> hashTags;

  public Tweet(String text, Set<String> hashTags) {
    this.text = text;
    this.hashTags = hashTags;
  }

  public String getText() {
    return text;
  }

  public Set<String> getHashTags() {
    return hashTags;
  }

  public String hashTagsToString() {
    return String.join(" ", getHashTags());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Tweet tweet = (Tweet) o;
    return Objects.equals(text, tweet.text) &&
        Objects.equals(hashTags, tweet.hashTags);
  }

  @Override
  public int hashCode() {
    return Objects.hash(text, hashTags);
  }

  @Override
  public String toString() {
    return "Tweet{" +
        "text='" + text + '\'' +
        ", hashTags=" + hashTags +
        '}';
  }
}
