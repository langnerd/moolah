package com.langnerd.moolah.model;

public enum Sentiment {
  VERY_NEGATIVE,
  NEGATIVE,
  NEUTRAL,
  POSITIVE,
  VERY_POSITIVE
}
