package com.langnerd.moolah.model;

import java.util.Objects;

public class WeightedSentiment {

  private final Sentiment sentiment;

  private final SentenceScore sentenceScore;

  private final int weight;

  public WeightedSentiment(int sentiment, int weight, SentenceScore sentenceScore) {
    this.sentiment = Sentiment.values()[sentiment];
    this.weight = weight;
    this.sentenceScore = sentenceScore;
  }

  public Sentiment getSentiment() {
    return sentiment;
  }

  public int getSentimentOrdinal() {
    return sentiment.ordinal();
  }

  public SentenceScore getSentenceScore() {
    return sentenceScore;
  }

  public int getWeight() {
    return weight;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WeightedSentiment that = (WeightedSentiment) o;
    return weight == that.weight &&
        sentiment == that.sentiment &&
        Objects.equals(sentenceScore, that.sentenceScore);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sentiment, sentenceScore, weight);
  }

  @Override
  public String toString() {
    return "WeightedSentiment{" +
        "sentiment=" + sentiment +
        ", sentenceScore=" + sentenceScore +
        ", weight=" + weight +
        '}';
  }
}
