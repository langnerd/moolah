package com.langnerd.moolah.model;

import java.util.List;
import java.util.Objects;

public class TweetSentiment {

  private final Sentiment sentiment;

  private final List<SentenceScore> sentenceScores;

  public TweetSentiment(Sentiment sentiment,
      List<SentenceScore> sentenceScores) {
    this.sentiment = sentiment;
    this.sentenceScores = sentenceScores;
  }

  public Sentiment getSentiment() {
    return sentiment;
  }

  public List<SentenceScore> getSentenceScores() {
    return sentenceScores;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TweetSentiment that = (TweetSentiment) o;
    return sentiment == that.sentiment &&
        Objects.equals(sentenceScores, that.sentenceScores);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sentiment, sentenceScores);
  }

  @Override
  public String toString() {
    return "TweetSentiment{" +
        "sentiment=" + sentiment +
        ", sentenceScores=" + sentenceScores +
        '}';
  }
}
