package com.langnerd.moolah.model;

import java.util.Objects;

public class ScoreDistribution {

  private final double veryPositive;
  private final double positive;
  private final double neutral;
  private final double negative;
  private final double veryNegative;

  public ScoreDistribution(double veryPositive, double positive, double neutral, double negative,
      double veryNegative) {
    this.veryPositive = veryPositive;
    this.positive = positive;
    this.neutral = neutral;
    this.negative = negative;
    this.veryNegative = veryNegative;
  }

  public double getVeryPositive() {
    return veryPositive;
  }

  public double getPositive() {
    return positive;
  }

  public double getNeutral() {
    return neutral;
  }

  public double getNegative() {
    return negative;
  }

  public double getVeryNegative() {
    return veryNegative;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScoreDistribution that = (ScoreDistribution) o;
    return Double.compare(that.veryPositive, veryPositive) == 0 &&
        Double.compare(that.positive, positive) == 0 &&
        Double.compare(that.neutral, neutral) == 0 &&
        Double.compare(that.negative, negative) == 0 &&
        Double.compare(that.veryNegative, veryNegative) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(veryPositive, positive, neutral, negative, veryNegative);
  }

  @Override
  public String toString() {
    return "ScoreDistribution{" +
        "veryPositive=" + veryPositive +
        ", positive=" + positive +
        ", neutral=" + neutral +
        ", negative=" + negative +
        ", veryNegative=" + veryNegative +
        '}';
  }
}
