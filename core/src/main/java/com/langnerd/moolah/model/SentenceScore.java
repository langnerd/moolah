package com.langnerd.moolah.model;

import java.util.Objects;

public class SentenceScore {

  private final Sentiment sentiment;

  private final ScoreDistribution scoreDistribution;

  public SentenceScore(int sentiment,
      ScoreDistribution scoreDistribution) {
    this.sentiment = Sentiment.values()[sentiment];
    this.scoreDistribution = scoreDistribution;
  }

  public Sentiment getSentiment() {
    return sentiment;
  }

  public ScoreDistribution getScoreDistribution() {
    return scoreDistribution;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SentenceScore that = (SentenceScore) o;
    return sentiment == that.sentiment &&
        Objects.equals(scoreDistribution, that.scoreDistribution);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sentiment, scoreDistribution);
  }

  @Override
  public String toString() {
    return "SentenceScore{" +
        "sentiment=" + sentiment +
        ", scoreDistribution=" + scoreDistribution +
        '}';
  }
}
