CREATE SCHEMA `moolah` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `prices`;
CREATE TABLE `prices` (
`trading_day` DATE NOT NULL,
`day_open` DECIMAL(19,7) NOT NULL,
`day_high` DECIMAL(19,7) NOT NULL,
`day_low` DECIMAL(19,7) NOT NULL,
`day_close` DECIMAL(19,7) NOT NULL,
`day_close_adj` DECIMAL(19,7) NOT NULL,
`volume` INT NOT NULL
);

DROP TABLE IF EXISTS `tweets`;
CREATE TABLE `tweets` (
`created_at` DATETIME NOT NULL,
`tweet_id` BIGINT NOT NULL,
`tweet_text` TEXT NOT NULL,
`tweet_source` VARCHAR(255) NULL,
`truncated` BOOL NOT NULL,
`retweet_count` INT NOT NULL,
`favorite_count` INT NOT NULL,
`tweet_lang` VARCHAR(3) NOT NULL
);

