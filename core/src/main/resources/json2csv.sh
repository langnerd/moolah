#!/bin/bash

# Gather daily tweets per stock (https://bit.ly/2Q5bz9F) into a single JSON
ls -dh ./tweet/raw/* | xargs -n 1 basename | xargs -I {} \
bash -c "cat ./tweet/raw/{}/* > ./out/{}.json | cat ./out/* > tweets.json"

# Convert selected JSON fields into CSV
cat tweets.json \
| jq -r \
'[.created_at,
  .id_str,
  .text,
  .source,
  .retweet_count,
  .favorite_count,
  .lang] | @csv' \
> tweets.csv
