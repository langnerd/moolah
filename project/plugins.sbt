addSbtPlugin("com.twilio" % "sbt-guardrail" % "0.59.0")
addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3")
addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
