import sbt.Keys.libraryDependencies
import sbt._

object Dependencies {

  val stanfordNLP = Seq(
    "edu.stanford.nlp" % "stanford-corenlp" % "4.0.0",
    "edu.stanford.nlp" % "stanford-corenlp" % "4.0.0" % "models"
  )

  val akka = Seq(
    "com.typesafe.akka" %% "akka-http" % "10.1.7",
    "com.typesafe.akka" %% "akka-stream" % "2.5.19"
  )

  val http4s = Seq(
    "org.http4s" %% "http4s-blaze-server",
    "org.http4s" %% "http4s-blaze-client",
    "org.http4s" %% "http4s-circe",
    "org.http4s" %% "http4s-dsl")
    .map(_ % "0.21.5")

  val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"

  val specs2 = "org.specs2" %% "specs2-core" % "4.10.0" % "test"

  object Cats {
    val akka = Seq("org.typelevel" %% "cats-effect" % "1.4.0")
    val http4s = Seq("org.typelevel" %% "cats-effect" % "2.1.0")
  }

  object Circe {
    private val deps = Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser"
    )
    val akka = deps.map(_ % "0.12.3")
    val http4s = deps.map(_ % "0.13.0")
  }

}
